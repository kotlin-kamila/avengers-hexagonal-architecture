package one.digitalinnovation.avengers.avengersapi.resource.avenger

import one.digitalinnovation.avengers.avengersapi.domain.avenger.Avenger
import javax.persistence.*

/*
 * Camada de infraestrutura.
 * Representa dados na tabela relacionada a Entidade Avenger
 */
@Entity
data class AvengerEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long?,
        @Column(nullable = false)
        val nick: String,
        @Column(nullable = false)
        val person: String,
        val description: String?,
        val history: String?
) {

    fun toAvenger() = Avenger(id, nick, person, description, history)

    companion object {
        fun from(avenger: Avenger) = AvengerEntity(
                id = avenger.id,
                nick = avenger.nick,
                person = avenger.person,
                description = avenger.description,
                history = avenger.history
        )
    }
}
