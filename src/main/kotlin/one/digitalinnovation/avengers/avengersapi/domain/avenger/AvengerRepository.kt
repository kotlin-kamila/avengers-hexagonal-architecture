package one.digitalinnovation.avengers.avengersapi.domain.avenger

// Porta, interface implementada por componente injetável
interface AvengerRepository {

    fun getAvengers(): List<Avenger>
    fun getDetail(id: Long): Avenger?
    fun create(avenger: Avenger): Avenger
    fun delete(id: Long)
    fun update(avenger: Avenger):Avenger

}
