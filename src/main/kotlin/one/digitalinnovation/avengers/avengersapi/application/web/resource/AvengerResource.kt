package one.digitalinnovation.avengers.avengersapi.application.web.resource

import one.digitalinnovation.avengers.avengersapi.application.web.resource.request.AvengerRequest
import one.digitalinnovation.avengers.avengersapi.application.web.resource.response.AvengerResponse
import one.digitalinnovation.avengers.avengersapi.domain.avenger.AvengerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import javax.validation.Valid

private const val API_PATH = "/v1/api/avenger"

@RestController
@RequestMapping(API_PATH)
class AvengerResource(
        @Autowired private val repository: AvengerRepository
) {

    @GetMapping
    fun getAvengers() = repository.getAvengers()
            .map { AvengerResponse.from(it) } // it: Avenger
            .let {
                ResponseEntity.ok().body(it) // it: List<AvengerResponse>, resultado do .map
            }

    @GetMapping("{id}/detail")
    fun getAvengerDetails(@PathVariable("id") id: Long) =
            repository.getDetail(id)?.let {
                ResponseEntity.ok().body(AvengerResponse.from(it))
            } ?: ResponseEntity.notFound().build<Void>()


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createAvenger(@Valid @RequestBody request: AvengerRequest) =
            request.toAvenger().run {
                repository.create(this) // this = resposta do toAvenger disponibilizada pelo run
            }.let {
                ResponseEntity.created(URI("$API_PATH/${it.id}"))
            }

    @PutMapping("{id}")
    fun updateAvenger(@Valid @RequestBody request: AvengerRequest, @PathVariable("id") id: Long) =
            repository.getDetail(id)?.let {
                AvengerRequest.to(it.id, request).apply {
                    repository.update(this)
                }.let { avenger ->
                    ResponseEntity.ok().body(AvengerResponse.from(avenger))
                }
            } ?: ResponseEntity.notFound().build<Void>()


    @DeleteMapping("{id}")
    fun deleteAvenger(@PathVariable("id") id: Long) =
            repository.delete(id).let {
                ResponseEntity.accepted().build<Void>()
            }

}
