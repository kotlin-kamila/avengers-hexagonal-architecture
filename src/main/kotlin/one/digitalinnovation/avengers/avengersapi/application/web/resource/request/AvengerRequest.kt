package one.digitalinnovation.avengers.avengersapi.application.web.resource.request

import one.digitalinnovation.avengers.avengersapi.domain.avenger.Avenger
import javax.validation.constraints.NotBlank

data class AvengerRequest(

        @field:NotBlank
        val nick: String,

        @field:NotBlank
        val person: String,

        val description: String? = null,

        val history: String? = null
) {
    // método acessado via instância. Com atribuições via named parameter
    fun toAvenger() = Avenger(
            nick = nick,
            person = person,
            description = description,
            history = history
    )

    // static function, acessada via classe, e não via instância
    companion object {
        fun to(id: Long?, request: AvengerRequest) = Avenger(
                id = id,
                nick = request.nick,
                person = request.person,
                description = request.description,
                history = request.history
        )
    }
}
