# Avengers API

Projeto criado durante o curso "Introdução à Arquitetura Hexagonal com Spring Boot e Kotlin" através da plataforma Digital Innovation One.

Github base: https://github.com/General-Studies/dio-avengers-api


`Procfile` arquivo necessário para Heroku.

Migrations do `Flyway` nomeadas com padrão timestamp: `VYYYYMMDDhhmmss__migration_name.sql`. 

# Scripts
Script / Comandos
> docker-compose -f backend-services.yaml up -d (deploy) / docker-compose -f backend-services.yaml down (undeploy)

Start API
> ./mvnw spring-boot:run -Dspring-boot.run.profiles=dev -Dspring-boot.run.jvmArguments="-Xmx256m -Xms128m" -Dspring-boot.run.arguments="'--DB_USER=dio.avenger' '--DB_PASSWORD=dio.avenger' '--DB_NAME=avengers'"

### Developer
[Kamila Serpa](https://kamilaserpa.github.io)

[1]: https://www.linkedin.com/in/kamila-serpa/
[2]: https://gitlab.com/kamilas

[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)][1]
[![linkedin](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)][2]
